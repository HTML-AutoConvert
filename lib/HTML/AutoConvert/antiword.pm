package HTML::AutoConvert::antiword;

=head1 NAME

HTML::AutoConvert::antiword - antiword plugin for HTML::AutoConvert

=head1 URL

Antiword can be downloaded from http://www.winfield.demon.nl/index.html

=cut

use strict;
use vars qw( %info );
use base 'HTML::AutoConvert::Run';

%info = (
  'types'   => 'doc',
  'weight'  => 90,
  'url'     => 'http://www.winfield.demon.nl/index.html',
);

sub program { ( 'antiword' ) }

sub html_convert {
  my $self = shift;
  my $html = $self->SUPER::html_convert(@_);
  "<HTML><HEAD></HEAD><BODY><PRE>\n$html\n</PRE></BODY></HTML>";
}

1;
