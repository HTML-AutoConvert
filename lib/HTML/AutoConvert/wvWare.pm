package HTML::AutoConvert::wvWare;

=head1 NAME

HTML::AutoConvert::wvWare - wvWare plugin for HTML::AutoConvert

=head1 URL

wvWare can be downloaded from http://wvware.sourceforge.net/

=cut

use strict;
use vars qw( %info );
use base 'HTML::AutoConvert::Run';

%info = (
  'types'   => 'doc',
  'weight'  => 80,
  'url'     => 'http://wvware.sourceforge.net/',
);

sub program { ( 'wvWare' ) }

1;
