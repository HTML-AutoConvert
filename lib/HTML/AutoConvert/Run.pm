package HTML::AutoConvert::Run;

=head1 NAME

HTML::AutoConvert::Run - Base class for HTML::AutoConvert plugs that run an external program

=cut

use strict;
use IPC::Run qw( run timeout );

sub html_convert {
  my( $self, $file ) = ( shift, shift );
  my $opt = ref($_[0]) ? shift : { @_ };

  my @program = $self->program;
  my $program = $program[0];

  my $timeout = 60; #?

  my( $html, $err ) = ( '', '');
  local($SIG{CHLD}) = sub {};
  run( [ @program, $file ], \undef, \$html, \$err, timeout($timeout) )
    or die "$program failed with exit status ". ( $? >> 8 ). ": $err\n";

  $html;

}

1;
