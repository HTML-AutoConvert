package HTML::AutoConvert::unrtf;

=head1 NAME

HTML::AutoConvert::unrtf - unrtf plugin for HTML::AutoConvert

=head1 URL

unrtf can be downloaded from ftp://ftp.gnu.org/pub/gnu/unrtf/

=cut

use strict;
use vars qw( %info );
use base 'HTML::AutoConvert::Run';

%info = (
  'types'   => 'rtf',
  'weight'  => 90,
  'url'     => 'ftp://ftp.gnu.org/pub/gnu/unrtf/',
);

sub program { ( 'unrtf' ) }

1;
