#!perl

BEGIN { chomp($pwd=`pwd`); $ENV{PATH} .= ":$pwd/bin"; };

use Test::More tests => 2;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $force = 'OpenOffice';
#$c->{'handlers'}{'doc'}{$force}{'weight'} = -1;
my @del = grep { $_ ne $force } keys %{ $c->{'handlers'}{'rtf'} };
delete($c->{'handlers'}{'rtf'}{$_}) foreach @del;

my $html = $c->html_convert('t/VEGAN_RECIPES.rtf');

#match $html against something;
like( $html, qr/Gumbo/, 'text has Gumbo' );
like( $html, qr/Cheesecake/, 'text has Cheesecake' );

