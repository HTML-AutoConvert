#!perl

use Test::More tests => 9;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $force = 'poppler';
#$c->{'handlers'}{'doc'}{$force}{'weight'} = -1;
my @del = grep { $_ ne $force } keys %{ $c->{'handlers'}{'pdf'} };
delete($c->{'handlers'}{'pdf'}{$_}) foreach @del;

my( $html, @images ) = $c->html_convert('t/attitude.pdf');

ok( scalar(@images) == 21, 'got 21 images' );

#save em off
#foreach my $image (@images) {
#  my( $file, $data) = @$image;
#  open(FILE, ">t/$file") or die $!;
#  print FILE $data;
#  close FILE or die $!;
#}

#check the names & lengths at least
is( $images[0]->[0], '000.ppm', '1st image name');
ok( length($images[0]->[1]) == 25949, '1st image size');

is( $images[1]->[0], '001.ppm', '2nd image name');
ok( length($images[1]->[1]) == 43664, '1st image size');

is( $images[2]->[0], '002.ppm', '3rd image name');
ok( length($images[2]->[1]) == 46833, '1st image size');

is( $images[9]->[0], '009.ppm', '10th image name');
ok( length($images[9]->[1]) == 46374, '10th image size');

