#!perl

BEGIN { chomp($pwd=`pwd`); $ENV{PATH} .= ":$pwd/bin"; };

use Test::More tests => 5;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $force = 'OpenOffice';
#$c->{'handlers'}{'doc'}{$force}{'weight'} = -1;
my @del = grep { $_ ne $force } keys %{ $c->{'handlers'}{'doc'} };
delete($c->{'handlers'}{'doc'}{$_}) foreach @del;

my( $html, @images ) = $c->html_convert('t/HeatherElko.doc');

ok( scalar(@images) == 2, 'got two images' );

#save em off
#foreach my $image (@images) {
#  my( $file, $data) = @$image;
#  open(FILE, ">t/$file") or die $!;
#  print FILE $data;
#  close FILE or die $!;
#}

#check the names & lengths at least
#well, the names don't appear to be consistent, but we can check they're png
like( $images[0]->[0], qr/^[0-9A-F]+.png$/, '1st image name');
ok( length($images[0]->[1]) == 8704, '1st image size');

like( $images[0]->[0], qr/^[0-9A-F]+.png$/, '2nd image name');
ok( length($images[1]->[1]) == 2125, '2nd image size');


