#!perl

use Test::More tests => 2;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $force = 'antiword';
#$c->{'handlers'}{'doc'}{$force}{'weight'} = -1;
my @del = grep { $_ ne $force } keys %{ $c->{'handlers'}{'doc'} };
delete($c->{'handlers'}{'doc'}{$_}) foreach @del;

my $html = $c->html_convert('t/DiaryofaKillerCat.doc');

#match $html against something;
like( $html, qr/Chunky/, 'text is Chunky' );
like( $html, qr/Monkey/, 'text has Monkey' );

