#!perl

BEGIN { chomp($pwd=`pwd`); $ENV{PATH} .= ":$pwd/bin"; };

use Test::More tests => 2;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $html = $c->html_convert('t/DiaryofaKillerCat.doc');

#match $html against something;
like( $html, qr/Chunky/, 'text is Chunky' );
like( $html, qr/Monkey/, 'text has Monkey' );

