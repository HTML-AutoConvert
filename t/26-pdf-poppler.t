#!perl

use Test::More tests => 2;

use HTML::AutoConvert;

my $c = new HTML::AutoConvert;

my $force = 'poppler';
#$c->{'handlers'}{'doc'}{$force}{'weight'} = -1;
my @del = grep { $_ ne $force } keys %{ $c->{'handlers'}{'pdf'} };
delete($c->{'handlers'}{'pdf'}{$_}) foreach @del;

my $html = $c->html_convert('t/attitude.pdf');

#match $html against something;
like( $html, qr/sampling/, 'text has sampling' );
like( $html, qr/respondents/, 'text has respondents' );

