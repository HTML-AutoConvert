#!perl -T

use Test::More tests => 1;

BEGIN {
	use_ok( 'HTML::AutoConvert' );
}

diag( "Testing HTML::AutoConvert $HTML::AutoConvert::VERSION, Perl $], $^X" );
