HTML-AutoConvert

This module is intended for best-effort auto-conversion of arbitrary files
into HTML.

The main focus is on conversion of Microsoft Word .DOC and .RTF file as well
as Adobe PDF.  The OpenOffice plugin also converts .SXW and .ODT formats.

The actual conversion is mostly done by shelling out to existing programs.
Useful programs to have installed:

For DOC/RTF/SXW/ODT:
OpenOffice v2.3 or later
Python and Python-UNO

For DOC:
wvWare http://wvware.sourceforge.net/
antiword http://www.winfield.demon.nl/index.html

For RTF:
unrtf ftp://ftp.gnu.org/pub/gnu/unrtf/

For PDF:
poppler http://poppler.freedesktop.org/

INSTALLATION

To install this module, run the following commands:

	perl Makefile.PL
	make
	make test
	make install

SUPPORT AND DOCUMENTATION

After installing, you can find documentation for this module with the
perldoc command.

    perldoc HTML::AutoConvert

You can also look for information at:

    RT, CPAN's request tracker
        http://rt.cpan.org/NoAuth/Bugs.html?Dist=HTML-AutoConvert

    AnnoCPAN, Annotated CPAN documentation
        http://annocpan.org/dist/HTML-AutoConvert

    CPAN Ratings
        http://cpanratings.perl.org/d/HTML-AutoConvert

    Search CPAN
        http://search.cpan.org/dist/HTML-AutoConvert


COPYRIGHT AND LICENCE

Copyright (C) 2008 Freeside Internet Services, Inc.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

HTML::AutoConvert::OpenOffice.pm derived from "PyODConverter"
<http://www.artofsolving.com/opensource/pyodconverter>
Copyright (C) 2008 Mirko Nasato <mirko@artofsolving.com>
Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl-2.1.html
- or any later version.

